package com.gitlab.chkypros.jackson;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

@SpringBootApplication
public class JacksonApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(JacksonApplication.class);
    }


    @Override
    public void run(String... args) throws Exception {
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            final String next = scanner.next();
            if ("exit".equals(next)) {
                break;
            }

            handle(next);
        }
    }

    private void handle(String filePath) throws IOException {
        final Path path = Paths.get(filePath);
        final byte[] originalBytes = Files.readAllBytes(path);
        final String tis620String = new String(originalBytes, "TIS620");
        final byte[] utf8String = tis620String.getBytes(StandardCharsets.UTF_8);


        try (final OutputStream outputStream = Files.newOutputStream(path.resolveSibling("utf8." + path.getFileName()))) {
            outputStream.write(utf8String);
        }
    }
}
